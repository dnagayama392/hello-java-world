/* 
 * simpleBBS.js
 * simpleBBS.html に 投稿情報を出力する jQuery.
 */

// APIからの投稿情報を整形し、表示
function formatPosts(data){
    var $post_div = $("<div/>");
    $(data).find("post").each(function() {
        var $post = $(this);
        var $name    = $post.find('name').text();
        var $content = $post.find('content').text();
        var $image   = $post.find('image').text();
        // 表示前処理
        $content = $content.replace(/((https?|ftp):\/\/[\w/:%#\$&\?~\.=\+\-]+)/g, "<a href=\"$1\" target\"_blank\">$1</a>");
        $content = $content.replace(/\r\n/g,"<br />");
        // コンソールで変数の中身を確認
//        console.log("$name: "+ $name);
//        console.log("$content: "+ $content);
//        console.log("$image: "+ $image);
        // 投稿テーブル記述
        var $restable = $("<table/>");
        $restable.attr("class", "post");
        var $tr = $("<tr/>");
        // 画像表示
        if($image != "")
        {
            var $imageArea = $("<td/>");
            $imageArea.attr("class","imagearea");
            var $img = $("<img/>");
            $img.attr("src", "./GetImage?file=" + $image);
            $tr.append($imageArea.append($img));
        }
        // テキスト表示
        var $textArea = $("<td/>");
        $textArea.attr("class","textarea");
        //// 投稿者名の表示
        var $text_head_div = $("<div/>");
        $text_head_div.attr("class","post_head");
        $text_head_div.append($name);
        $textArea.append($text_head_div);
        //// 投稿内容の表示
        var $text_content_div = $("<div/>");
        $text_content_div.attr("class","post_content");
        $text_content_div.append($content);
        $textArea.append($text_content_div);
        // テキスト部をtrに追加
        $tr.append($textArea);
        // tr を テーブルに追加
        $restable.append($tr);
        // 投稿単位の追加
        $post_div.append($("<hr/>"));
        $post_div.append($restable);
    });
    // 子要素を空にしてから追加
    $("#result").empty();
    $post_div.appendTo("#result");
}

$(function() {
    $.ajax( {
        url: "/HelloWorld/SimpleBBSAPI",
        type: 'GET',
        success: formatPosts
    });
    
    $('#submit_form').submit(function(event){
        // HTMLでの送信をキャンセル
        event.preventDefault();
        // 操作対象のフォーム要素を取得
        var $form = $(this);
        // 送信ボタンを取得
        var $button = $form.find('#submit_post');
        
//        var $postData = $form.serialize();
//        console.log("$postData: "+ $postData);
        var $fd = new FormData(document.getElementById("submit_form"));
        console.log("$fd: "+ $fd);
            
        // 送信
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $fd,
            timeout: 10000, // 単位はミリ秒
            processData: false,  // jQuery がデータを処理しないよう指定
            contentType: false,  // jQuery が contentType を設定しないよう指定
            // 送信前
            beforeSend: function(xhr, settings) {
                // ボタンを無効化し、二重送信を防止
                $button.attr('disabled', true);
            },
            // 応答後
            complete: function(xhr, textStatus) {
                // ボタンを有効化し、再送信を許可
                $button.attr('disabled', false);
            },
            // 通信成功時の処理
            success: function(xhr, textStatus, error) {
//                alert('Success');
                $.ajax( {
                    url: "/HelloWorld/SimpleBBSAPI",
                    type: 'GET',
                    success: formatPosts
                });
            },
            // 通信失敗時の処理
            error: function(xhr, textStatus, error) {
                alert('通信失敗しました。');
            }
        });
    });
});