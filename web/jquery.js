/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
            
//$(function(){
////    alert("準備完了");
////    $("div").css("background", "#555");
//    $("#links").load("/HelloWorld/ #index_title");
//    $("button").click(function ()
//    {
//        $.get("/HelloWorld/BBSAPI/", function (myData, myStatus){
//            $("p").text(myData);W
//        });
//        alert("準備完了");
//    });
//});

$(document).ready(function(){
    // 偶数行の色を設定
    $('table#stripe-table tr:even').css('background-color', '#666');
    // 奇数行の色を設定
    $('table#stripe-table tr:odd').css('background-color', '#333');
});

//変数titleScrを宣言
var titleScr = html=[], realcnt=0;
//titleタグの中
var titleScr = /<title>(.*)<\/title>/;
//jQueryでindex.htmlのタイトルをget

function myCallback(data){
    var titleStr = titleScr.exec(data)[1];
    //h1の内容を置き換え
    $("h1").html(titleStr);
}

$.get("./index.html", myCallback);

$(function() {
    $('#result').click(function(){
        $(this).empty();
    });
    
    $.ajax( {
        url: "/HelloWorld/SimpleBBSAPI",
        type: 'GET',
        success:function(data) {
            var $post_div = $("<div/>");
            $(data).find("post").each(function() {
                var $post = $(this);
                var $name    = $post.find('name').text();
                var $content = $post.find('content').text();
                var $image   = $post.find('image').text();
                // コンソールで変数の中身を確認
//                console.log("$name: "+ $name);
//                console.log("$content: "+ $content);
//                console.log("$image: "+ $image);
                // 投稿テーブル記述
                var $restable = $("<table/>");
                $restable.attr("class","Response");
                var $tr = $("<tr/>");
                // 画像表示
                if($image != "")
                {
                    var $imageArea = $("<td/>");
                    $imageArea.attr("class","imagearea");
                    var $img = $("<img/>");
                    $img.attr("src", "./GetImage?file=" + $image);
                    $tr.append($imageArea.append($img));
                }
                // テキスト表示
                var $textArea = $("<td/>");
                $textArea.attr("class","textarea");
                //// 投稿者名の表示
                var $text_head_div = $("<div/>");
                $text_head_div.attr("class","responseHead");
                $text_head_div.append($name);
                $textArea.append($text_head_div);
                //// 投稿内容の表示
                var $text_content_div = $("<div/>");
                $text_content_div.attr("class","responseContent");
                $text_content_div.append($content);
                $textArea.append($text_content_div);
                // テキスト部をtrに追加
                $tr.append($textArea);
                // tr を テーブルに追加
                $restable.append($tr);
                // 投稿単位の追加
                $post_div.append($("<hr/>"));
                $post_div.append($restable);
            });
            $post_div.appendTo("#result");
        }
    });
});
