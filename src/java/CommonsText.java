
import java.io.IOException;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 文章表現用定数クラス
 * @author nagayama
 */
public class CommonsText
{
    public static final String CONTENT_HTML_UTF8  = "text/html;charset=UTF-8";
    public static final String CONTENT_XML_UTF8   = "text/xml;charset=UTF-8";
    public static final String ENPTY = "";
    public static final String INDENT_UNIT = "    ";
    public static final String LFCR = "\r\n";
    public static final String MATCH_URL = "(https?|ftp)://[\\w/:%#\\$&\\?~\\.=\\+\\-]+";
    public static final String MATCH_URL_E = "\r\n";
    
    /**
     * 文字列が指定の最大値以内か判定。
     * @param target 対象文字列
     * @param maxLength 最大長
     * @param errorMessage IOException に持たせるエラーメッセージ
     * @throws IOException 文字列が指定の最大値より大きいとき投げられる。
     */
    public static void checkMaxLength(String target, int maxLength, String errorMessage)
            throws IOException
    {
        if(target.length() > maxLength)
        {
            throw new IOException(errorMessage);
        }
    }
    
    /**
     * 入力値分のインデントを返す。
     * @param i
     * @return 
     */
    public static String makeIndent(int i)
    {
        String ind;
        ind = multiply(INDENT_UNIT, i);
        return ind;
    }
    
    /**
     * 文字列を数値分だけ繰り返して返す
     * @param str
     * @param num
     * @return 
     */
    public static String multiply(String str, int num)
    {
        String mulStr = ENPTY;
        int count;
        for(count = 0; count < num; count++)
        {
            mulStr += str;
        }        
        return mulStr;
    }
    
    /**
     * 文字列のhtml部分にaタグを付けて返す。
     * @param tx
     * @return 
     */
    public static String repliceHyperlink(String tx)
    {
        tx = tx.replaceAll("(https?|ftp)://[\\w/:%#\\$&\\?~\\.=\\+\\-]+", "<a href=\"$0\" target\"_blank\">$0</a>");
        return tx;
    }
    
    /**
     * HTMLにとって危険な文字を文字参照に置換(&amp;, &lt;, &gt;, &quot;, &#39;)。
     * @param str 対象文字列。
     * @return 置換された文字列。
     */
    public static String repliceWarningWords(String str)
    {
        str = str.replaceAll("&", "&amp;");
        str = str.replaceAll(">", "&gt;");
        str = str.replaceAll("<", "&lt;");
        str = str.replaceAll("\"", "&quot;");
        str = str.replaceAll("'", "&#39;");
        str = str.replaceAll("\\?", "&#63;");
        return str;
    }
}
