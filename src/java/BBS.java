/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * BBSページ用サーブレット
 * @author nagayama
 */
@WebServlet(urlPatterns = {"/BBS"})
public class BBS extends HttpServlet
{
    private static final String CONTENT_UTF8  = CommonsText.CONTENT_HTML_UTF8;
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType(CONTENT_UTF8);
        PrintWriter out = response.getWriter();
        ArrayList<HTMLLine> htmlList = new ArrayList<HTMLLine>();
        htmlList.clear();
        try
        {
            ResponseManager resManager = new ResponseManager();
            ArrayList<HTMLLine> postedValue = resManager.getAllResList();
            htmlList.add(new HTMLLine(0, "<!DOCTYPE html>"));
            htmlList.add(new HTMLLine(0, createAA()));
            htmlList.add(new HTMLLine(1, "<html>"));
            htmlList.add(new HTMLLine(1, "<head>"));
            htmlList.add(new HTMLLine(0, "<meta http-equiv=\"Content-Type\" content=\"" + CONTENT_UTF8 + "\">"));
            htmlList.add(new HTMLLine(0, "<link rel=\"stylesheet\" href=\"./hello.css\" type=\"text/css\">"));
            htmlList.add(new HTMLLine(0, "<link rel=\"stylesheet\" href=\"./bbs.css\" type=\"text/css\">"));
            htmlList.add(new HTMLLine(0, "<link rel=\"icon\" type=\"image/png\" href=\"./world.ico\">"));
            htmlList.add(new HTMLLine(0, "<title>ながやまＢＢＳ</title>"));            
            htmlList.add(new HTMLLine(-1, "</head>"));
            htmlList.add(new HTMLLine(1, "<body>"));
            htmlList.add(new HTMLLine(0, "<div id=\"main\">"));
            htmlList.add(new HTMLLine(0, "<div id=\"title\">ながやまＢＢＳ</div>"));
            htmlList.add(new HTMLLine(0, "<div align=\"right\">[<a href=\"./\">TOPへ</a>]</div>"));
            htmlList.add(new HTMLLine(1, "<form method=\"post\" action=\"Entry\" enctype=\"multipart/form-data\">"));
            htmlList.add(new HTMLLine(1, "<table align=\"center\">"));
            htmlList.add(new HTMLLine(1, "<tr><td>"));
            htmlList.add(new HTMLLine(1, "<table align=\"center\">"));
            htmlList.add(new HTMLLine(1, "<tr>"));
            htmlList.add(new HTMLLine(0, "<td><b>名前</b></td>"));
            htmlList.add(new HTMLLine(1, "<td>"));
            htmlList.add(new HTMLLine(0, "<input id=\"username\" name=\"username\" type=\"text\" size=\"64\" maxlength=\"64\">"));
            htmlList.add(new HTMLLine(-1, "</td>"));
            htmlList.add(new HTMLLine(-1, "</tr>"));
            htmlList.add(new HTMLLine(1, "<tr>"));
            htmlList.add(new HTMLLine(0, "<td><b>E-Mail</b></td>"));
            htmlList.add(new HTMLLine(0, "<td><input id=\"email\" name=\"email\" type=\"text\" size=\"256\" maxlength=\"256\" /></td>"));
            htmlList.add(new HTMLLine(-1, "</tr>"));
            htmlList.add(new HTMLLine(1, "<tr>"));
            htmlList.add(new HTMLLine(0, "<td><b>内容</b></td>"));
            htmlList.add(new HTMLLine(1, "<td>"));
            htmlList.add(new HTMLLine(0, "<textarea name=\"message\" cols=\"60\" rows=\"6\" onkeypress=\"return (this.value.length <= 512);\" maxlength=\"512\"></textarea>"));
            htmlList.add(new HTMLLine(-1, "</td>"));
            htmlList.add(new HTMLLine(-1, "</tr>"));
            htmlList.add(new HTMLLine(0, "<tr><td><b>添付File</b></td><td><input type=\"file\" name=\"datafile\"></td></tr>"));
            htmlList.add(new HTMLLine(0, "<tr><td></td><td id=\"submit_cell\"><input id=\"post_response\" type=\"submit\" value =\"送信\"></td></tr>"));
            htmlList.add(new HTMLLine(-1, "</table>"));
            htmlList.add(new HTMLLine(-1, "</td></tr>"));
            htmlList.add(new HTMLLine(0, "<tr><td><hr></td></tr>"));
            htmlList.add(new HTMLLine(1, "<tr><td>"));
            htmlList.add(new HTMLLine(1, "<ul>"));
            htmlList.add(new HTMLLine(0, "<li class=\"notes\">画像ファイル(jpg, png, gif)をアップロードできます。</li>"));
            htmlList.add(new HTMLLine(0, "<li class=\"notes\">アップロードできるファイルのサイズは512KB未満です。</li>"));
            htmlList.add(new HTMLLine(0, "<li class=\"notes\">投稿、コメント内のurlはオートリンクされます。</li>"));
            htmlList.add(new HTMLLine(0, "<li class=\"notes\">名前は64文字以下、内容は512文字以下でお願いします。</li>"));
            htmlList.add(new HTMLLine(-1, "</ul>"));
            htmlList.add(new HTMLLine(-1, "</td></tr>"));
            htmlList.add(new HTMLLine(-1, "</table>"));
            htmlList.add(new HTMLLine(-1, "</form>"));
            htmlList.addAll(postedValue);
            htmlList.add(new HTMLLine(0, "<hr>"));
            htmlList.add(new HTMLLine(0, "<p align=\"right\">&copy 2013 D.Nagayama.</p>"));
            htmlList.add(new HTMLLine(-1, "</div>"));
            htmlList.add(new HTMLLine(-1, "</body>"));
            htmlList.add(new HTMLLine(-1, "</html>"));
            HTMLMaker htmlMaker = new HTMLMaker();
            String prihtml = htmlMaker.create(htmlList);
            /* TODO output your page here. You may use following sample code. */
            out.print(prihtml);
        }
        finally
        {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.sendRedirect("./BadRequest");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
    
    private String createAA()
    {
        String unit = "";
        unit += "<!--\r\n";
        unit += "　●ヽ( ･ω･`)ﾉ●\r\n";
        unit += "　●ヽ(･ω･`)ﾉ●\r\n";
        unit += "　　●(ω･`ﾉ●\r\n";
        unit += "　　　(･`● )\r\n";
        unit += "　　 (●　　)ﾉ●\r\n";
        unit += "　 ●ヽ(　　 )ﾉ●\r\n";
        unit += "　　●(　 ´)ﾉ●\r\n";
        unit += "　　　(　´ﾉ●\r\n";
        unit += "　　　( ﾉ● )\r\n";
        unit += "　　　●´･ω)\r\n";
        unit += "　 ●ヽ( ･ω･)●\r\n";
        unit += "　●ヽ( ･ω･`)ﾉ●\r\n";
        unit += "　●ヽ(･ω･`)ﾉ●\r\n";
        unit += "　　●(ω･`ﾉ●\r\n";
        unit += "　　 (･`ﾉ● )\r\n";
        unit += "　　 (●　　)ﾉ●\r\n";
        unit += "　 ●ヽ(　 ´)ﾉ●\r\n";
        unit += "　　●(　 ´)ﾉ●\r\n";
        unit += "　　　(　´ﾉ●\r\n";
        unit += "　　　( ﾉ● )\r\n";
        unit += "　　　●´･ω)\r\n";
        unit += "　 ●ヽ( ･ω･)●\r\n";
        unit += "　●ヽ( ･ω･`)ﾉ● < いくでっ！\r\n";
        unit += "-->\r\n";
        return unit;
    }
}
