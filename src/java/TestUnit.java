/**
 * TestUnit.java: 単体テスト用
 */

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nagayama
 */
public class TestUnit
{
    public static void main(String[] args)
    {
        System.out.println("Hello, World");
        ArrayList<String> result = new ArrayList<>();
        // 接続オブジェクト
        HttpURLConnection http;
        http = createGETURLConection("http://192.168.5.40:8081/HelloWorld/SimpleBBSAPI");
        try {
            // 接続
            http.connect();
            // XML 取得
            // Documentオブジェクトを取得
            Document doc = getXMLDoc(http);
            // ここから先は一般的な DOM の処理
            // ルート要素
            Element root = doc.getDocumentElement();
            // System.out.println(root.getNodeName());
            System.out.println(root.getNodeName());

            // getElementsByTagName が一番直感的で確実
            NodeList postList = root.getElementsByTagName("post");
            System.out.println(Integer.toString(postList.getLength()));
            // 一つ目のノード
            Node post1 = getAloneNode(root,"post");
            System.out.println(post1.getNodeName());
            
            // getElementsByTagName が一番直感的で確実
            NodeList contentList = ((Element)post1).getElementsByTagName("content");
            System.out.println(Integer.toString(contentList.getLength()));
            // 一つ目のノード
            Node content1 = getAloneNode(post1,"content");
            System.out.println(content1.getNodeName());

            System.out.println(getTextValue(content1));
            
            result.add(getTextValue(content1));
        }
        catch (IOException ex) // connect に失敗しました。
        {
            Logger.getLogger(TestUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            // 接続解除
            http.disconnect();
        }
        for(String str:result)
        {
            System.out.println(str);
        }
    }
    
    private static Node getAloneNode(Element element, String childName)
    {
        Node childNode = null;
        if(childName==null)
        {
            return null;
        }
        NodeList nodeList = element.getElementsByTagName(childName);
        if(nodeList.getLength() > 0)
        {
            childNode = nodeList.item(0);
        }
        return childNode;
    }

    private static Node getAloneNode(Node node, String childName)
    {
        return getAloneNode((Element)node, childName);
    }
        
    public static URL createURL(String urlPath)
    {
        URL url = null;
        try {
            // ターゲット
            url = new URL(urlPath);
        }
        catch (MalformedURLException ex) // 不正な形式の URL です
        {
            Logger.getLogger(TestUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
        return url;
    }
    
    public static HttpURLConnection createGETURLConection(String urlPath)
    {
        HttpURLConnection http = null;
        try
        {
            // ターゲット
            URL url = createURL(urlPath);
            // 接続オブジェクト
            http = (HttpURLConnection)url.openConnection();
            // GET メソッド
            http.setRequestMethod("GET");
        }
        catch (IOException ex) // openConnection に失敗しました。
        {
            Logger.getLogger(TestUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
        return http;
    }
    
    public static String getTextValue(Node node)
    {
        String text = null;
        if(node == null)
        {
            return null;
        }
        Node txNode = node.getFirstChild();
        if(txNode == null)
        {
            return null;
        }
        if(txNode.getNodeType() == Node.TEXT_NODE)
        {
            text = txNode.getNodeValue();
        }
        return text;
    }

    public static Document getXMLDoc(HttpURLConnection http)
    {
        Document doc = null;
        try
        {
            // XML 取得の準備
            DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbfactory.newDocumentBuilder();
            // InputStream から Documentオブジェクトを取得
            doc = builder.parse(http.getInputStream());
        }
        catch (ParserConfigurationException ex)
        {
            Logger.getLogger(TestUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (SAXException ex)
        {
            Logger.getLogger(TestUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            return doc;
        }
    }
    
}
