/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * コメントの追加と追加結果を表示するサーブレット
 * @author nagayama
 */
@WebServlet(urlPatterns = {"/EntryComment"})
public class EntryComment extends HttpServlet
{

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType(CommonsText.CONTENT_HTML_UTF8);
        PrintWriter out = response.getWriter();
        
        boolean isSuccess = false;
        try
        {
            // 日時情報の定義
            SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            // 各アイテムの取得
            String strParentID  = new String(request.getParameter("comment_parentID").getBytes("iso-8859-1"), "UTF-8");
            int parentID  = Integer.valueOf(strParentID);
            String rcDate  = datetime.format(date).toString();
            String author  = new String(request.getParameter("comment_author").getBytes("iso-8859-1"), "UTF-8");
            String content  = new String(request.getParameter("comment_content").getBytes("iso-8859-1"), "UTF-8");
            // 投稿コメントの追加
            ResComment rc = new ResComment(parentID, rcDate, author, content);
            ResCommentManager rcManager = new ResCommentManager();
            isSuccess = rcManager.insertResComment(rc);
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<meta http-equiv=\"Content-Type\" content=\"" + CommonsText.CONTENT_HTML_UTF8 + "\">");
            out.println("<link rel=\"stylesheet\" href=\"./hello.css\" type=\"text/css\">");
            out.println("<link rel=\"stylesheet\" href=\"./bbs.css\" type=\"text/css\">");
            out.println("<link rel=\"icon\" type=\"image/png\" href=\"./world.ico\">");
            out.println("<title>コメント投稿確認 - ながやまBBS</title>");            
            out.println("</head>");
            out.println("<body>");
            if(isSuccess)
            {
                out.println("<h1>コメントの投稿が完了しました</h1>");
                
                out.println("<table>");
                out.println("<tr>");
                out.println("<td>投稿日時</td>");
                out.println("<td>" + rcDate + "</td>");
                out.println("</tr>");
                out.println("<tr>");
                out.println("<td>名前</td>");
                out.println("<td>" + author + "</td>");
                out.println("</tr>");
                out.println("<td>内容</td>");
                out.println("<td>" + content + "</td>");
                out.println("<tr>");
                out.println("</tr>");
                out.println("</table>");
            }
            else
            {
                out.println("<h1>コメントの投稿に失敗しました</h1>");
                out.println("<p>次のような原因が考えられます</p>");
                out.println("<ul><li>コメント内容を記入していない。</li></ul>");
            }
            out.println("<form action=\"#\">");
            out.println("<input type=\"button\" value=\"閉じる\" onclick=\"window.close();\">");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
        finally
        {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.sendRedirect("./BadRequest");
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
