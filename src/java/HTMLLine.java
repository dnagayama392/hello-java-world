/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * HTML一行あたりのテキスト情報を保持
 * @author nagayama
 */
public class HTMLLine
{
    /* メンバ変数 */
    private int indent;
    private String content;

    /* getter, setter */
    public int getIndent()
    {
        return indent;
    }

    public void setIndent(int indent)
    {
        if(this.indent != indent)
        {
            this.indent = indent;
        }
    }

    public String getContent()
    {
        return content;
    }

    private void setContent(String content)
    {
        if(content == null)
        {
            content = CommonsText.ENPTY;
        }
        if(this.content == null)
        {
            this.content = content;
        }
        if(!this.content.equals(content))
        {
            this.content = content;
        }
    }
    
    /* public 関数 */
    public HTMLLine(int indent, String content)
    {
        Initialize(indent, content);
    }
    
    /* private 関数 */
    private void Initialize(int indent, String content)
    {
        setIndent(indent);
        setContent(content);
    }
}
