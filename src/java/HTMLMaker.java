
import java.util.ArrayList;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nagayama
 */
public class HTMLMaker
{
    public HTMLMaker()
    {
        
    }
    
    public String create(ArrayList<HTMLLine> htmlList)
    {
        String htmlStr = CommonsText.ENPTY;
        int indent = 0;
        int lIndent = 0;
        for(HTMLLine line : htmlList)
        {
            lIndent = line.getIndent();
            if(lIndent < 0)
            {
                indent = renewIndent(indent, lIndent);
            }
            htmlStr += createIndent(indent) + line.getContent() + CommonsText.LFCR;
            if(lIndent > 0)
            {
                indent = renewIndent(indent, lIndent);
            }
        }
        return htmlStr;
    }
    
    private String createIndent(int indent)
    {
        String strIndent;
        strIndent = CommonsText.multiply(CommonsText.INDENT_UNIT, indent);
        return strIndent;
    }
    
    private int renewIndent(int indent, int lIndent)
    {
        indent += lIndent;
        if(indent < 0)
        {
            indent = 0;
        }
        return indent;
    }
}