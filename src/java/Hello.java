/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nagayama
 */
@WebServlet(urlPatterns = {"/Hello"})
public class Hello extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            String[] mathematician={
                "チャールズ・ダーウィン",
                "アルベルト・アインシュタイン",
                "コーキー・ロバーツ",
                "ピエール・ド・フェルマー",
                "バートランド・ラッセル",
                "リチャード・ファインマン",
                "ジョン・ケリー"
            };
            String[] word={
                "数学者とは、真っ暗な部屋の中で、そこには居ない黒猫を探し続ける人のようなものだ。",
                "数学の法則で自然を表そうとしても、決して正確に表すことはできない。また、どんなに正確な法則であっても、現実とはかけ離れている。",
                "学校で、幾何学が教え続けられる限り、学校で祈る人はいなくならないだろう。",
                "私はこの問題の素晴らしい証明方法を思いついたが、それを書くにはこの余白は狭すぎる。",
                "それだから数学は、何について話しているのか我々が決して知りえず、話していることが真かどうか我々が決して知りえない学問として定義される。",
                "もし数学というものがすべて消えてなくなったとしたら、物理学の進歩はちょうど一週間、遅れるだろう。",
                "ドーナツとコーヒーカップの違いが分からない人のことを、位相幾何（トポロジー）学者という。"
            };
            int rdm = (int)(java.lang.Math.random()*1000);
            int index = rdm % mathematician.length;
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<meta http-equiv='content-type' content='text/html; charset=UTF-8' />");
            out.println("<link rel='icon' type='image/png' href='./world.ico'>");
            out.println("<title>Hello, Java world!</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h2>Hello, Java world!</h2>");
            out.println("<form method='post' action='Entry'>");
            out.println("<div>");
            out.println("<p>");
            out.println("名前： ");
            out.println("<input name='username' type='text' size='24' value='" + mathematician[index] + "'>");
            out.println("</p>");
            out.println("</div>");
            out.println("<div>");
            out.println("<textarea name='message' cols='60' rows='6'>" + word[index] + "</textarea>");
            out.println("</div>");
            out.println("<input type='submit' value='送信'>");
            out.println("</form>");
            out.println("<div><image src='./bgt.gif'></div>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
