
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * コメントの入出力管理クラス
 * @author nagayama
 */
public class ResCommentManager
{
    private static final String LFCR = CommonsText.LFCR;
    private static final String MYSQL_DRIVER = CommonsDB.MYSQL_DRIVER;
    private static final String SERV_URL     = CommonsDB.SERV_URL;
    private static final String SERV_USER    = CommonsDB.SERV_USER;
    private static final String SERV_PASS    = CommonsDB.SERV_PASS;
    private static final String TABLE         = "comments_tbl";
    private static final String ROW_ID        = "id";
    private static final String ROW_PARENT_ID = "parentID";
    private static final String ROW_DATETIME  = "commentDate";
    private static final String ROW_AUTHOR    = "author";
    private static final String ROW_CONTENT   = "content";
    
    public ResCommentManager()
    {
        
    }
    
    /**
     * 受け取ったコメント一覧を表示するHTML文字列を作成。
     * @param resComments 対象のコメント一覧
     * @return 
     */
    public String createComentDivision(ArrayList<ResComment> resComments)
    {
        String postedValue ="";
        for(ResComment comment : resComments)
        {
            String author = comment.getAuthor();
            String content = comment.getContent();
            String datetime = comment.getCommentDate();
            postedValue += "<div class=\"comment\">" + LFCR;
            
            postedValue += "<div class=\"comment_content\">" + content + "</div>" + LFCR;
            postedValue += "<div class=\"comment_footer\">" + LFCR;
            postedValue += "<span class=\"comment_author\">-&nbsp;" + author + "</span>" + LFCR;
            postedValue += "<span class=\"comment_date\">" + datetime + "</span>" + LFCR;
            postedValue += "</div>" + LFCR;
            postedValue += "</div>" + LFCR;
        }
        return postedValue;
    }

    /**
     * コメント投稿のフォームタグを作成
     * @param id コメント先の投稿id
     * @return 
     */
    public String createCommentEntryForm(int id)
    {
        String commentEntryForm;
        commentEntryForm = "";
        commentEntryForm += "<form method=\"post\" action=\"EntryCommentForm\" target=\"_blank\">";
        commentEntryForm += "<input type=\"submit\" name=\"comment_submit\" value =\"コメント\">";
        commentEntryForm += "<input type=\"hidden\" name=\"comment_parentID\" value =\"" + id + "\">";
        commentEntryForm += "</form>" + LFCR;
        return commentEntryForm;
    }
    
    /**
     * 投稿IDに対するコメント一覧を返す
     * @param resID 投稿ID
     * @return ArrayList<ResComment>
     */
    public ArrayList<ResComment> getResComments(int resID)
    {
        ArrayList<ResComment> resComments = new ArrayList();
        resComments.clear();
        try
        {
            // ドライバクラスをロード
            Class.forName(MYSQL_DRIVER); // MySQLの場合
            // データベースへ接続
            Connection connection = DriverManager.getConnection(SERV_URL, SERV_USER, SERV_PASS);
            // データの取得
            String parentID = Integer.toString(resID);
            String sql = "SELECT * FROM " + TABLE + " WHERE " + ROW_PARENT_ID + " = " + parentID + ";";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            // データの挿入
            ResComment resComment;
            while(resultSet.next())
            {
                int id = resultSet.getInt(ROW_ID);
                int paID = resultSet.getInt(ROW_PARENT_ID);
                String comDate = resultSet.getString(ROW_DATETIME);
                comDate  = convToCommentDate(comDate);
                String auth = resultSet.getString(ROW_AUTHOR);
                String cont = resultSet.getString(ROW_CONTENT);
                auth = auth.replace("<", "&lt;");
                auth = auth.replace(">", "&gt;");
                cont = cont.replace("<", "&lt;");
                cont = cont.replace(">", "&gt;");
                resComment = new ResComment(id, paID, comDate, auth, cont);
                resComments.add(resComment);
            }
            // 接続解除
            resultSet.close();
            statement.close();
            connection.close();
        }
        catch (ClassNotFoundException | SQLException ex)
        {
            Logger.getLogger(TestPage.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resComments;
    }
    
    /**
     * コメントをDBに追加（追加できなければfalse）
     * @param resComment 追加するコメント
     * @return 
     */
    public boolean insertResComment(ResComment resComment)
    {
        boolean isSuccess = false;
        if(resComment.getContent().isEmpty())
        {
            return false;
        }
        try
        {
            // ドライバクラスをロード
            Class.forName("org.gjt.mm.mysql.Driver"); // MySQLの場合
            // データベースへ接続
            Connection connection = DriverManager.getConnection(SERV_URL, SERV_USER, SERV_PASS);
            // データを追加するSQL文の作成
            String sql;
            sql = "INSERT INTO " + TABLE + " (parentID, commentDate, author, content) VALUES (?, ?, ?, ?)";
            PreparedStatement prepStmt = connection.prepareStatement(sql);
            // 各列の値を取得
            String parentID    = Integer.toString(resComment.getParentID());
            String commentDate = resComment.getCommentDate();
            String author      = resComment.getAuthor();
            String content     = resComment.getContent();
            // 雛形を実際の値に置き換えSQL文を実行
            prepStmt.setString(1, parentID);
            prepStmt.setString(2, commentDate);
            prepStmt.setString(3, author);
            prepStmt.setString(4, content);
            prepStmt.execute();
            // 接続を閉じる
            prepStmt.close();
            connection.close();
            // 成功判定の切り替え
            isSuccess = true;
        }
        catch (ClassNotFoundException ex)
        {
            Logger.getLogger(ResCommentManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ResCommentManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            return isSuccess;
        }
    }
    
    /**
     * datetime型の時刻表記を整形
     * @param datetime
     * @return 
     */
    private String convToCommentDate(String datetime)
    {
        String commentDateTime;
        commentDateTime = "";
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy'-'MM'-'dd' 'HH':'mm':'ss");
            Date date = sdf.parse(datetime);
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy'/'MM'/'dd'('E') 'HH':'mm':'ss");
            commentDateTime  = sdf2.format(date).toString();
        }
        catch (ParseException ex)
        {
            Logger.getLogger(TestPage.class.getName()).log(Level.SEVERE, null, ex);
        }
        return commentDateTime;
    }
}
