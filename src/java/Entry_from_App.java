/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author nagayama
 */
@WebServlet(urlPatterns = {"/Entry_from_App"})
@MultipartConfig(fileSizeThreshold = 5000000, maxFileSize = 10000000, location = "C:\\Data")
public class Entry_from_App extends HttpServlet {

    private static final String PRIVATE_REFERER = CommonsBBS.PRIVATE_REFERER;
    private static final int NAME_MAX_LENGTH    = CommonsBBS.NAME_MAX_LENGTH;
    private static final int EMAIL_MAX_LENGTH   = CommonsBBS.EMAIL_MAX_LENGTH;
    private static final int CONTENT_MAX_LENGTH = CommonsBBS.CONTENT_MAX_LENGTH;
    private static final String NAME_OVER_MAX_ERROR_MESSAGE = CommonsBBS.NAME_OVER_MAX_ERROR_MESSAGE;
    private static final String EMAIL_OVER_MAX_ERROR_MESSAGE = CommonsBBS.EMAIL_OVER_MAX_ERROR_MESSAGE;
    private static final String CONTENT_OVER_MAX_ERROR_MESSAGE = CommonsBBS.CONTENT_OVER_MAX_ERROR_MESSAGE;
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(CommonsText.CONTENT_HTML_UTF8);
        PrintWriter out = response.getWriter();
        try {
            checkRight(request);
            //日時情報の定義
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy'/'MM'/'dd'('E')' HH:mm:ss");
            Date submitedDate = new Date();
            //各アイテムの取得
            String time  = dateFormat.format(submitedDate).toString();
            String name  = CommonsBBS.getParameter("username", request);
            String email = CommonsBBS.getParameter("email", request);
            String text  = CommonsBBS.getParameter("message", request);
            String file  = CommonsText.ENPTY;
            // 例外処理 （長すぎる場合 IOException）
            CommonsText.checkMaxLength(name, NAME_MAX_LENGTH, NAME_OVER_MAX_ERROR_MESSAGE);
            CommonsText.checkMaxLength(email, EMAIL_MAX_LENGTH, EMAIL_OVER_MAX_ERROR_MESSAGE);
            CommonsText.checkMaxLength(text, CONTENT_MAX_LENGTH, CONTENT_OVER_MAX_ERROR_MESSAGE);
            // 画像追加
            // <INPUT type="file" name="datafile"> で指定した名前より取得
            Part part = request.getPart("datafile");
            file  = CommonsBBS.uploadFile(part);
            // 投稿情報を追加
            Response addResponse =  new Response(time, name, text, email, file);
            ResponseManager resManager = new ResponseManager();
            // 追加成否による後処理
            if(!resManager.add(addResponse)) {
                out.println("<div>Error...XP</div>");
            } else {
                out.println("<div>Good!XD</div>");
            }
        } catch(IOException ex) {
            out.println("<div>" + ex.getMessage() + "</div>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private String getCookie(HttpServletRequest request, String key) {
        String cookieVal = "";
        Cookie cookie[] = request.getCookies();
        if(cookie != null) {
            for(int i=0; i<cookie.length ; i++) {
                if(cookie[i].getName().equals(key)) {
                    cookieVal = cookie[i].getValue();
                }
            }
        }
        return cookieVal;
    }
    
    private void checkRight(HttpServletRequest request)
            throws IOException
    {
        try {
            String referer = request.getHeader("REFERER");
            String cookie = getCookie(request, "token");        //httpRequestヘッダからcookieを取得
            String token = new String(request.getParameter("token").getBytes("iso-8859-1"), "UTF-8");
            if(!(referer.equals(PRIVATE_REFERER) && cookie.equals(token))) {
                throw new IOException();
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Entry_from_App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
