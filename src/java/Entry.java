/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;// Date
import java.text.*;// SimpleDateFormat
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * 投稿をするサーブレット
 * @author nagayama
 */
@WebServlet(urlPatterns = {"/Entry"})
@MultipartConfig(fileSizeThreshold = 5000000, maxFileSize = 10000000, location = "C:\\Data")
public class Entry extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // JavaEE 7 では効いてないが、慣例的に以下のメソッドを利用することが多いとのことので残します
        //request.setCharacterEncoding("UTF-8");
        response.setContentType(CommonsText.CONTENT_HTML_UTF8);
        PrintWriter out = response.getWriter();
        try {
            // CSRF攻撃にされされてるか判定
            checkRight(request);
            // 日時情報の定義
            SimpleDateFormat date1 = new SimpleDateFormat("yyyy'/'MM'/'dd'('E')' HH:mm:ss");
            Date submitedDate = new Date();
            // 各アイテムの取得
            String time  = date1.format(submitedDate).toString();
            String name  = CommonsBBS.getParameter("username", request);
            String email = CommonsBBS.getParameter("email", request);
            String text  = CommonsBBS.getParameter("message", request);
            String file  = CommonsText.ENPTY;
            // 例外処理 （長すぎる場合 IOException）
            CommonsText.checkMaxLength(name, CommonsBBS.NAME_MAX_LENGTH, CommonsBBS.NAME_OVER_MAX_ERROR_MESSAGE);
            CommonsText.checkMaxLength(email, CommonsBBS.EMAIL_MAX_LENGTH, CommonsBBS.EMAIL_OVER_MAX_ERROR_MESSAGE);
            CommonsText.checkMaxLength(text, CommonsBBS.CONTENT_MAX_LENGTH, CommonsBBS.CONTENT_OVER_MAX_ERROR_MESSAGE);
            // 画像追加
            // <INPUT type="file" name="datafile"> で指定した名前より取得
            Part part = request.getPart("datafile");
            file  = CommonsBBS.uploadFile(part);
           
            // 投稿情報を追加
            Response addResponse =  new Response(time, name, text, email, file);
            ResponseManager resManager = new ResponseManager();
            boolean error = !resManager.add(addResponse);
            // 追加成否による後処理
            if(error) {
                out.println("<div>Error</div>");
                response.sendRedirect("./BadRequest");
            } else {
                response.sendRedirect("./BBS");
            }
        } catch(IOException ex) {
            Logger.getLogger(Entry.class.getName()).log(Level.SEVERE, null, ex);
            out.println("<div>"+ex.getMessage()+"</div>");
        } catch(ServletException ex) {
            Logger.getLogger(Entry.class.getName()).log(Level.SEVERE, null, ex);
            out.println("<div>"+ex.getMessage()+"</div>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.sendRedirect("./BadRequest");
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
    
    /**
     * REFERER が BBS でなければ IOExeption
     * @param request
     * @throws IOException 
     */
    private void checkRight(HttpServletRequest request)
            throws IOException
    {
        try {
            String referer = request.getHeader("REFERER");
            if(!(referer.equals(CommonsBBS.PRIVATE_REFERER))) {
                throw new IOException();
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Entry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
