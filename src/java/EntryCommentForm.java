/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * コメント入力ページのサーブレット
 * @author nagayama
 */
@WebServlet(urlPatterns = {"/EntryCommentForm"})
public class EntryCommentForm extends HttpServlet
{

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        String parentID  = new String(request.getParameter("comment_parentID").getBytes("iso-8859-1"), "UTF-8");
        PrintWriter out = response.getWriter();
        try
        {
            ResponseManager resManager = new ResponseManager();
            Response res = resManager.getResponse(Integer.valueOf(parentID));
            ArrayList<HTMLLine> postedLines = new ArrayList<>();
            postedLines.clear();
            /* output page here. */
            postedLines.add(new HTMLLine(0, "<!DOCTYPE html>"));
            postedLines.add(new HTMLLine(1, "<html>"));
            postedLines.add(new HTMLLine(1, "<head>"));
            postedLines.add(new HTMLLine(0, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"));
            postedLines.add(new HTMLLine(0, "<link rel=\"stylesheet\" href=\"./bbs.css\" type=\"text/css\">"));
            postedLines.add(new HTMLLine(0, "<link rel=\"stylesheet\" href=\"./bbs.css\" type=\"text/css\">"));
            postedLines.add(new HTMLLine(0, "<link rel=\"icon\" type=\"image/png\" href=\"./world.ico\">"));
            postedLines.add(new HTMLLine(0, "<title>コメント投稿 - ながやまＢＢＳ</title>"));
            postedLines.add(new HTMLLine(-1, "</head>"));
            postedLines.add(new HTMLLine(1, "<body>"));
            postedLines.add(new HTMLLine(0, "<div>次の投稿にコメントします。</div>"));
            postedLines.add(new HTMLLine(1, "<table>"));
            postedLines.add(new HTMLLine(1, "<tr><td style=\"background-color: white; margin 5px;\">"));
            postedLines.add(new HTMLLine(0, resManager.createResponseTable(0, res)));
            postedLines.add(new HTMLLine(-1, "</td></tr>"));
            postedLines.add(new HTMLLine(1, "<tr><td style=\"background-color: white; margin 5px;\">"));
            postedLines.add(new HTMLLine(0, "<form method=\"post\" action=\"EntryComment\">"));
            postedLines.add(new HTMLLine(0, "<input type=\"text\" name=\"comment_author\" placeholder=\"名前\" style=\"width:8em;\" size=\"64\" maxlength=\"64\">"));
            postedLines.add(new HTMLLine(0, "<!DOCTYPE html>"));
            postedLines.add(new HTMLLine(0, "<!DOCTYPE html>"));
            
            out.println(indentTag(0) + "<!DOCTYPE html>");
            out.println(indentTag(0) + "<html>");
            out.println(indentTag(1) + "<head>");
            out.println(indentTag(2) + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
            out.println(indentTag(2) + "<link rel=\"stylesheet\" href=\"./hello.css\" type=\"text/css\">");
            out.println(indentTag(2) + "<link rel=\"stylesheet\" href=\"./bbs.css\" type=\"text/css\">");
            out.println(indentTag(2) + "<link rel=\"icon\" type=\"image/png\" href=\"./world.ico\">");
            out.println(indentTag(2) + "<title>コメント投稿 - ながやまＢＢＳ</title>");
            out.println(indentTag(1) + "</head>");
            out.println(indentTag(1) + "<body>");
            out.println(indentTag(2) + "<div>次の投稿にコメントします。</div>");
            out.println(indentTag(2) + "<table>");
            out.println(indentTag(3) + "<tr><td style=\"background-color: white; margin 5px;\">");
            out.println(indentTag(4) + resManager.createResponseTable(0, res));
            out.println(indentTag(3) + "</td></tr>");
            out.println(indentTag(3) + "<tr><td style=\"background-color: white; margin 5px;\">");
            out.println(indentTag(4) + "<form method=\"post\" action=\"EntryComment\">");
            out.println(indentTag(5) + "<input type=\"text\" name=\"comment_author\" placeholder=\"名前\" style=\"width:8em;\" size=\"64\" maxlength=\"64\">");
            out.println(indentTag(5) + "<input type=\"text\" name=\"comment_content\" placeholder=\"コメント\" style=\"width:20em;\" size=\"256\" maxlength=\"256\">");
            out.println(indentTag(5) + "<input type=\"submit\" name=\"comment_submit\" value=\"コメント\">");
            out.println(indentTag(5) + "<input type=\"hidden\" name=\"comment_parentID\" value =\"" + parentID + "\">");
            out.println(indentTag(4) + "</form>");
            out.println(indentTag(3) + "</td></tr>");
            out.println(indentTag(2) + "</table>");
            out.println(indentTag(1) + "</body>");
            out.println(indentTag(0) + "</html>");
        }
        finally
        {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.sendRedirect("./BadRequest");
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
    
    private String indentTag(int i)
    {
        String ind;
        ind = CommonsText.makeIndent(i);
        return ind;
    }
}
