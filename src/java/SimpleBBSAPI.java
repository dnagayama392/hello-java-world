/*
 * SimpleBBSAPI.java
 *   BBSの投稿情報をXMLで返します。
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author nagayama
 */
@WebServlet(urlPatterns = {"/SimpleBBSAPI"})
public class SimpleBBSAPI extends HttpServlet
{
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try
        {
            // 表示オブジェクトの一覧取得
            ResponseManager resManager = new ResponseManager();
            ArrayList<Response> responses = resManager.getResponseAll();
            //DOM Documentのインスタンスを生成するBuilderクラスの
            //インスタンスを取得する
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            //ビルダーからDOMを取得する
            Document document = builder.newDocument();
            //XMLのルート要素(userタグ)を追加する
            Element root = document.createElement("root");
            document.appendChild(root);
            // 使用要素列挙
            for(int i = responses.size() - 1; i >= 0; i--)
            {
                Response res = responses.get(i);
                //ルート要素に子ノード(lastnameタグ)を追加する
                Element post = document.createElement("post");
                root.appendChild(post);
                //respoに子ノード(nameタグ)を追加する
                Element name = document.createElement("name");
                name.setTextContent(res.getName());
                post.appendChild(name);
                //respoに子ノード(contentタグ)を追加する
                Element content = document.createElement("content");
                content.setTextContent(res.getText());
                post.appendChild(content);
                //respoに子ノード(imageタグ)を追加する
                Element image = document.createElement("image");
                image.setTextContent(res.getFile());
                post.appendChild(image);
            }
            //DOMの内容をクライアントに出力する
            TransformerFactory tfactory = TransformerFactory.newInstance();
            Transformer transformer = tfactory.newTransformer();
            transformer.transform(new DOMSource(document), new StreamResult(out));
        }
        catch (ParserConfigurationException ex)
        {
            Logger.getLogger(SimpleBBSAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (TransformerConfigurationException ex)
        {
            Logger.getLogger(SimpleBBSAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (TransformerException ex)
        {
            Logger.getLogger(SimpleBBSAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
