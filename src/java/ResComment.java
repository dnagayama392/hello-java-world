/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * コメントの雛形クラス
 * @author nagayama
 */
public class ResComment
{
    /* メンバ定数 */
    private static final int ZERO = 0;
    private static final String DEFAULT_NAME = "S.P.Jobs";
    private static final String DEFAULT_TEXT = CommonsText.ENPTY;
    /* メンバ変数 */
    private int id;
    private int parentID;
    private String commentDate;
    private String author;
    private String content;
    
    /* ゲッター・セッター */
    public int getId()
    {
        return id;
    }

    private void setId(int id)
    {
        this.id = id;
    }

    public int getParentID()
    {
        return parentID;
    }
    
    private void setParentID(int i)
    {
        if(i < ZERO)
        {
            i = ZERO;
        }
        if(parentID != i)
        {
            parentID = i;
        }
    }

    public String getCommentDate()
    {
        return commentDate;
    }

    private void setCommentDate(String commentDate)
    {
        this.commentDate = commentDate;
    }

    public String getAuthor()
    {
        return author;
    }

    private void setAuthor(String author)
    {
        if(author == null)
        {
            author = DEFAULT_NAME;
        }
        if(author.isEmpty())
        {
            author = DEFAULT_NAME;
        }
        if(this.author == null)
        {
            this.author = author;
        }
        if(!this.author.equals(author))
        {
            this.author = author;
        }
    }

    public String getContent()
    {
        return content;
    }

    private void setContent(String content)
    {
        if(content == null)
        {
            content = DEFAULT_TEXT;
        }
        if(this.content == null)
        {
            this.content = content;
        }
        if(!this.content.equals(content))
        {
            this.content = content;
        }
    }
    
    /**
     * 
     * @param parentID 親投稿ID
     * @param commentDate 投稿日時
     * @param author 投稿者名
     * @param content 投稿内容
     */
    public ResComment(int parentID, String commentDate, String author, String content)
    {
        setId(0);
        setParentID(parentID);
        setCommentDate(commentDate);
        setAuthor(author);
        setContent(content);
    }
    
    /**
     * 
     * @param id 投稿id
     * @param parentID 親投稿id
     * @param commentDate 投稿日時
     * @param author 投稿者名
     * @param content 投稿内容
     */
    public ResComment(int id, int parentID, String commentDate, String author, String content)
    {
        setId(id);
        setParentID(parentID);
        setCommentDate(commentDate);
        setAuthor(author);
        setContent(content);
    }
}
