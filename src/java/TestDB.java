/**
 * TestDB.java: データベース接続テスト用
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * データベース入出力テスト用クラス
 * @author nagayama
 */
public class TestDB
{
    public static void main(String[] args)
    {
        System.out.println("Hello, World");
        readDB();
        Response newResponse = new Response("2010/07/31(土) 09:38:16", "しおだれ", "ヽ(,,･ω･)ﾉ");
        insertDB(newResponse);
        readDB();
    }
    
    private static void insertDB(Response response)
    {
        try
        {
            // ドライバクラスをロード
            Class.forName("org.gjt.mm.mysql.Driver"); // MySQLの場合
            // データベースへ接続
            String url = "jdbc:mysql://localhost:3306/hellobbsdb?zeroDateTimeBehavior=convertToNull";
            Connection con = DriverManager.getConnection(url, "root", "0000");
            // データの追加
            Statement stmt1 = con.createStatement();
            String sql1;
            
            String time = response.getTime();
            String name = response.getName();
            String text = response.getText();
            
            sql1 = "insert into entry_tbl (time, name, text) ";
            sql1 += "values ('" + time + "', '" + name + "', '" + text + "')";
            stmt1.execute(sql1);
            
            stmt1.close();
            con.close();
        }
        catch (ClassNotFoundException | SQLException ex)
        {
            Logger.getLogger(ResponseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
        }
    }
    
    
    private static Response[] readDB()
    {
        try
        {
            // ドライバクラスをロード
            Class.forName("org.gjt.mm.mysql.Driver"); // MySQLの場合
            // データベースへ接続
            String url = "jdbc:mysql://localhost:3306/hellobbsdb?zeroDateTimeBehavior=convertToNull";
            Connection con = DriverManager.getConnection(url, "root", "0000");
            // データの取得
            Statement stmt2 = con.createStatement();
            String sql2 = "select * from entry_tbl";
            ResultSet rs2 = stmt2.executeQuery(sql2);
            ArrayList<Response> aryList = new ArrayList();
            aryList.clear();
            Response res;
            while(rs2.next()){
                String tm = rs2.getString("time");
                String nm = rs2.getString("name");
                String tx = rs2.getString("text");
                System.out.println(tm+":"+nm+":"+tx);
                res = new Response(tm, nm, tx);
                aryList.add(res);
            }
            rs2.close();
            Response[] resAry = aryList.toArray(new Response[aryList.size()]);
            // 接続解除
            stmt2.close();
            con.close();
            return resAry;
        }
        catch (ClassNotFoundException | SQLException ex)
        {
            System.out.println("Get Error");
            return null;
        }
    }
}
