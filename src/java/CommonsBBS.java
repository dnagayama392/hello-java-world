
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nagayama
 */
public class CommonsBBS {
    private static final long MAX_FILE_SIZE = 512 * 1024; // 単位：Byte
    public static final String PRIVATE_REFERER = "http://192.168.5.81:8081/HelloWorld/BBS";
    public static final int NAME_MAX_LENGTH    = 64;
    public static final int EMAIL_MAX_LENGTH   = 256;
    public static final int CONTENT_MAX_LENGTH = 512;
    public static final String NAME_OVER_MAX_ERROR_MESSAGE = "名前が長すぎます。64文字以下にしてください。";
    public static final String EMAIL_OVER_MAX_ERROR_MESSAGE = "メールアドレスが長すぎます。『RFC 2821 SMTP』の定義に基づき256文字以下にしてください。";
    public static final String CONTENT_OVER_MAX_ERROR_MESSAGE = "内容が長すぎます。512文字以下にしてください。";
    
    public static String getParameter(String paramName, HttpServletRequest request) {
        String paramater = CommonsText.ENPTY;
        try {
            paramater = new String(request.getParameter(paramName).getBytes("iso-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(CommonsBBS.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return paramater;
        }
    }
    
    /**
     * ファイルアップロードメソッド 
     * @param part
     * @return
     * @throws IOException 
     */
    public static String uploadFile(Part part)
            throws IOException {
        String file = CommonsText.ENPTY;
        try {
            // アップロードファイル情報を記録（Part確認用）
            FileWriter fw = new FileWriter(new File("c:\\data\\log.txt"));
            fw.write(part.toString());
            fw.close();
            // アップロードファイルが存在するならば保存
            if(part.getSize() != 0) {
                // ファイル名の取得
                String pname = getFilename(part);
                // ファイルサイズチェック
                if(!isPermissibleFileSize(part)) {
                    throw new IOException("ファイルのサイズが適切でありません。");
                }
                // ファイル拡張子チェック
                if(!isPictureFile(pname)) {
                    throw new IOException("ファイルの拡張子が適切でありません。");
                }
                // ファイルの保存 @MultipartConfig(location="C:\\Data") で設定した
                // ディレクトリ配下に保存される。
                part.write(pname);
                file  = pname;
            }
        } catch (IOException ex) {
            Logger.getLogger(CommonsBBS.class.getName()).log(Level.SEVERE, null, ex);
            throw new IOException(ex.getMessage());
        } finally {
            return file;
        }
    }
    
    /**
     * ファイル名の拡張子を返す。
     * @param filename ファイル名
     * @return 
     *     拡張子（String）: 拡張子を取得<br/>
     *     ファイル名（String）: 拡張子を取得不可<br/>
     *     null: ファイル名がnull
     */
    private static String getExtension(String filename) {
        String extension;
        if(filename == null) {
            return null;
        }
        int point = filename.lastIndexOf(".");
        if(point != -1) {
            extension = filename.substring(point + 1);
            return extension;
        }
        return filename;
    }
    
    /**
     * Part からファイル名を取得。
     * @param part
     * @return 
     */
    private static String getFilename(Part part) {
        for (String cd : part.getHeader("Content-Disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                return cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
        
    /**
     * 投稿されたファイルサイズが適切か判定
     * @param part
     * @return 
     */
    private static boolean isPermissibleFileSize(Part part) {
        if(part.getSize() < MAX_FILE_SIZE)
        {
            return true;
        }
        return false;
    }
    
    /**
     * 画像ファイルか判定します。
     * @param filename
     * @return 
     */
    private static boolean isPictureFile(String filename) {
        String extension = getExtension(filename);
        if(extension == null) {
            return false;
        }
        switch (extension) {
            case "png":
            case "PNG":
            case "jpg":
            case "JPG":
            case "jpeg":
            case "JPEG":
            case "gif":
            case "GIF":
                return true;
            default:
                return false;
        }
    }
}
