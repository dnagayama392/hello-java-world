/**
 * GetImage.java : 
 *     ?file=*** で *** の画像情報を返すサーブレット
 */

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.NullPointerException;

/**
 * 画像情報を取得するためのサーブレット
 * @author nagayama
 */
@WebServlet(urlPatterns = {"/GetImage"})
public class GetImage extends HttpServlet
{

    /**
     * Processes requests for both HTTP
     * <code>GET</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            response.setContentType("text/html;charset=UTF-8");
            /* HACK: 画像のパス以外を渡されたときに */
            /* 出力する画像ファイル名 */
            String imgfile = new String(request.getParameter("file").getBytes("iso-8859-1"), "UTF-8");
            String imgpath = "C:\\Data\\" + imgfile;
            int iData = 0;
            /* OutputStream を取得します。 */
            ServletOutputStream outStream = response.getOutputStream();

            /* 画像ファイルを BufferedInputStream を使用して読み取ります。*/
            BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(imgpath));

            /* 画像を書き出します。*/
            while ((iData = inStream.read()) != -1)
            {
                outStream.write(iData);
            }
            inStream.close();
            outStream.close();
        }
        catch(NullPointerException ex)
        {
            response.sendRedirect("./BadRequest");
        }
   }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.sendRedirect("./BadRequest");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
