/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 動的ページのテスト用サーブレット
 * @author nagayama
 */
@WebServlet(urlPatterns = {"/DynamicPageTest"})
public class TestPage extends HttpServlet
{
    private static final String CONTENT_UTF8 = CommonsText.CONTENT_HTML_UTF8;
    private static final String LFCR         = CommonsText.LFCR;
    private static final String SERV_URL     = CommonsDB.SERV_URL;
    private static final String SERV_USER    = CommonsDB.SERV_USER;
    private static final String SERV_PASS    = CommonsDB.SERV_PASS;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType(CONTENT_UTF8);
        PrintWriter out = response.getWriter();
        ArrayList<HTMLLine> htmlList = new ArrayList<HTMLLine>();
        htmlList.clear();
        try
        {
            htmlList.add(new HTMLLine(0, "<!DOCTYPE html>"));
            htmlList.add(new HTMLLine(1, "<html>"));
            htmlList.add(new HTMLLine(1, "<head>"));
            htmlList.add(new HTMLLine(0, "<meta http-equiv=\"Content-Type\" content=\"" + CONTENT_UTF8 + "\">"));
            htmlList.add(new HTMLLine(0, "<link rel=\"stylesheet\" href=\"./hello.css\" type=\"text/css\">"));
            htmlList.add(new HTMLLine(0, "<link rel=\"stylesheet\" href=\"./hello.css\" type=\"text/css\">"));
            htmlList.add(new HTMLLine(0, "<link rel=\"stylesheet\" href=\"./bbs.css\" type=\"text/css\">"));
            htmlList.add(new HTMLLine(0, "<link rel=\"icon\" type=\"image/png\" href=\"./world.ico\">"));
            htmlList.add(new HTMLLine(0, "<title>ながやまＢＢＳ</title>"));            
            htmlList.add(new HTMLLine(-1, "</head>"));
            htmlList.add(new HTMLLine(1, "<body>"));
            htmlList.add(new HTMLLine(0, "<div id=\"title\">ながやまＢＢＳ</div>"));
            htmlList.add(new HTMLLine(0, "<div align=\"right\">[<a href=\"./\">TOPへ</a>]</div>"));
            htmlList.add(new HTMLLine(0, createTestPost()));
            htmlList.add(new HTMLLine(0, "<hr>"));
            htmlList.add(new HTMLLine(0, "<p align=\"right\">&copy 2013 D.Nagayama.</p>"));
            htmlList.add(new HTMLLine(-1, "</body>"));
            htmlList.add(new HTMLLine(-1, "</html>"));
            HTMLMaker htmlMaker = new HTMLMaker();
            String prihtml = htmlMaker.create(htmlList);
            /* TODO output your page here. You may use following sample code. */
            out.print(prihtml);
        }
        finally
        {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.sendRedirect("http://trpg.scenecritique.com/Paranoia_O/");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
    
    private String createTestPost()
    {
        String postedValue = "";
        try
        {
            // （限定された）投稿一覧を取得
            ArrayList<Response> responses;
            responses = getResponses();
            Collections.reverse(responses);
            int i;
            i = responses.size();
            // 各投稿に対してコメント処理
            for(Response res : responses)
            {
                postedValue += createResponseTable(i, res);
                i--;
            }
            
        }
        finally
        {
            return postedValue;
        }
    }
    
    private String createResponseTable(int resNum, Response response)
    {
        String postedValue="";
        ResCommentManager rcManager = new ResCommentManager();
                // コメント取得
                int id = response.getId();
                ArrayList<ResComment> resComments = rcManager.getResComments(id);
                Collections.reverse(resComments);
                // 記述HTML
                postedValue += "<hr>" + LFCR;
                postedValue += "<table class=\"Response\">"+ LFCR;
                postedValue += "<tr>" + LFCR;
                if(!response.getFile().isEmpty())
                {
                    postedValue += "<td class=\"imagearea\">";
                    postedValue += "<img src=\"./GetImage?file=" + response.getFile() + "\">";
                    postedValue += "</td>" + LFCR;
                }
                postedValue += "<td class=\"textarea\">";
                postedValue += (resNum) + " ";
                if(response.getEmail().isEmpty())
                {
                    postedValue += response.getName();
                }
                else
                {
                    postedValue += "<a href=\"mailto:" + response.getEmail() + "\">";
                    postedValue += response.getName();
                    postedValue += "</a>";
                }
                postedValue += "&emsp;";
                postedValue += "" + response.getTime();
                
                postedValue += "<br>" + LFCR;
                postedValue += "<div class=\"responseContent\">" + LFCR;
                postedValue += response.getText().replace("\r\n", "<br>") + LFCR;
                // form メソッド
                postedValue += "<div class=\"commentPostField\">" + LFCR;
                postedValue += rcManager.createCommentEntryForm(id);
                postedValue += "</div>" + LFCR;
                postedValue += rcManager.createComentDivision(resComments);
                postedValue += "</div>" + LFCR;
                postedValue += "</td>" + LFCR;
                postedValue += "</table>" + LFCR;
                return postedValue;
    }
    
    /**
     * （限定した）投稿一覧を取得
     * @return ArrayList<Response>
     */
    private ArrayList<Response> getResponses()
    {
        ArrayList<Response> responses = new ArrayList();
        responses.clear();
        try
        {
            // ドライバクラスをロード
            Class.forName("org.gjt.mm.mysql.Driver"); // MySQLの場合
            // データベースへ接続
            Connection connection = DriverManager.getConnection(SERV_URL, SERV_USER, SERV_PASS);
            // データの取得
            String sql = "SELECT * FROM entry_tbl WHERE id = 131;";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            Response resp;
            while(resultSet.next())
            {
                int id     = resultSet.getInt("id");
                String tm  = resultSet.getString("time");
                String nm  = resultSet.getString("name");
                String tx  = resultSet.getString("text");
                String em  = resultSet.getString("email");
                String img = resultSet.getString("file");
                nm = nm.replace("<", "&lt;");
                nm = nm.replace(">", "&gt;");
                tx = tx.replace("<", "&lt;");
                tx = tx.replace(">", "&gt;");
                tx = CommonsText.repliceHyperlink(tx);
                resp = new Response(id, tm, nm, tx, em, img);
                responses.add(resp);
            }
            // 接続解除
            resultSet.close();
            statement.close();
            connection.close();
        }
        catch (ClassNotFoundException | SQLException ex)
        {
            Logger.getLogger(TestPage.class.getName()).log(Level.SEVERE, null, ex);
        }
        return responses;
    }
}
