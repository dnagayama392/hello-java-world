/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 投稿レスの雛形クラス
 * @author nagayama
 */
public class Response
{
    private static final String DEFAULT_TIME = "2112年09月03日 12:09:03";
    private static final String DEFAULT_NAME = "たろう";
    private static final String DEFAULT_TEXT = "";
    
    private int id;
    private String time;
    private String name;
    private String text;
    private String email;
    private String file;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        if(this.id == id)
        {
            return;
        }
        this.id = id;
    }
    
    public void setTime(String newTime)
    {
        if(newTime.isEmpty())
        {
            newTime = DEFAULT_TIME;
        }
        if(time == null)
        {
            time = newTime;
        }
        if(time.equals(newTime))
        {
           return;
        }
        time = newTime;
    }
    
    public String getTime()
    {
        return time;
    }
    
    public void setName(String newName)
    {
        if(newName.isEmpty())
        {
            newName = DEFAULT_NAME;
        }
        if(name == null)
        {
            name = newName;
        }
        if(name.equals(newName))
        {
           return;
        }
        name = newName;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setText(String newText)
    {
        if(newText.isEmpty())
        {
            newText = DEFAULT_TEXT;
        }
        if(text == null)
        {
            text = newText;
        }
        if(!text.equals(newText))
        {
            text = newText;
        }
    }
    
    public String getText()
    {
        return text;
    }
    
    public void setEmail(String newEmail)
    {
        if(newEmail.isEmpty())
        {
            newEmail = DEFAULT_TEXT;
        }
        if(email == null)
        {
            email = newEmail;
        }
        if(!email.equals(newEmail))
        {
            email = newEmail;
        }
    }
    
    public String getEmail()
    {
        return email;
    }
    
    public void setFile(String newFile)
    {
        if(newFile.isEmpty())
        {
            newFile = DEFAULT_TEXT;
        }
        if(file == null)
        {
            file = newFile;
        }
        if(file.equals(newFile))
        {
           return;
        }
        file = newFile;
    }
    
    public String getFile()
    {
        return file;
    }
    
    public Response()
    {
        initialize();
    }

    public Response(String time, String name, String text)
    {
        initialize(time, name, text);
    }

    public Response(String time, String name, String text, String email, String file)
    {
        initialize(time, name, text, email, file);
    }

    public Response(int id, String time, String name, String text, String email, String file)
    {
        initialize(id, time, name,  text,  email,  file);
    }

    private void initialize()
    {
        initialize(DEFAULT_TIME, DEFAULT_NAME,  DEFAULT_TEXT);
    }
    
    private void initialize(String time, String name, String text)
    {
        initialize(time, name,  text,  DEFAULT_TEXT,  DEFAULT_TEXT);
    }

    private void initialize(String time, String name, String text, String email, String file)
    {
        initialize(0, time, name,  text,  email,  file);
    }

    private void initialize(int id, String time, String name, String text, String email, String file)
    {
        setId(id);
        setTime(time);
        setName(name);
        setText(text);
        setEmail(email);
        setFile(file);
    }
}
