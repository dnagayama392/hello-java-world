/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.beans.*; /* XMLDecoder, XMLEncoder */
import java.io.*; /* BufferedInputStream, FileInputStream, FileNotFoundException */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*; /* ArrayList */

/**
 * 投稿レスの入出力管理クラス
 * @author nagayama
 */
public class ResponseManager
{
    private static final String ENPTY = CommonsText.ENPTY;
    private static final String LFCR  = CommonsText.LFCR;
    private static final String SERV_URL  = CommonsDB.SERV_URL;
    private static final String SERV_USER = CommonsDB.SERV_USER;
    private static final String SERV_PASS = CommonsDB.SERV_PASS;
    private static final String TABLE_ENTRY = "entry_tbl";
    private static final String BACKUP_FILE_NAME = "C:\\Data\\backup";
    private static final String BACKUP_EXTENSION = ".xml";
    private static final String XML_FILE_NAME = "C:\\Data\\responses.xml";
    
    public ResponseManager()
    {
        
    }
    
    /**
     * 投稿情報をDBに追加します。
     * @param addResponse
     * @return
     * @throws IOException 
     */
    public Boolean add(Response addResponse)
            throws IOException
    {
        // テキストが空なら無視
        if(addResponse.getText().equals(ENPTY))
        {
           throw new IOException("内容が空白なので投稿できません。"); 
        }
        // DBに追加
        if(insertResponse(addResponse))
        {
            makeBuckupFile();
            return true;
        }
        return false;
    }
    
    public String getAllResValue()
    {
        String postedValue = ENPTY;
        Response[] responseSet = selectAllResponses();
        if(responseSet == null)
        {
            return postedValue;
        }
        // 投稿単位表示
        for(int i = responseSet.length - 1; i >= 0; i--)
        {
            postedValue += "<hr>" + LFCR;
            postedValue += createResponseTable(i + 1, responseSet[i]);
        }
        return postedValue;
    }
    
    public ArrayList<HTMLLine> getAllResList()
    {
        ArrayList<HTMLLine> postedList = new ArrayList<HTMLLine>();
        postedList.clear();
        Response[] responseSet = selectAllResponses();
        if(responseSet == null)
        {
            return postedList;
        }
        // 投稿単位表示
        for(int i = responseSet.length - 1; i >= 0; i--)
        {
            postedList.add(new HTMLLine(0, "<hr>"));
            String resTable = createResponseTable(i + 1, responseSet[i]);
            postedList.add(new HTMLLine(0, resTable));
        }
        return postedList;
    }
    
    public String createResponseTable(int resNum, Response response)
    {
        // ローカル変数
        String postedValue = ENPTY;
        ResCommentManager rcManager = new ResCommentManager();
        // コメント取得
        int id = response.getId();
        ArrayList<ResComment> resComments = rcManager.getResComments(id);
//        Collections.reverse(resComments);
        // XSS回避処理
        response = escapeXSS(response);
        // 記述HTML
        postedValue += "<table class=\"Response\">"+ LFCR;
        postedValue += "<tr>" + LFCR;
        // 画像表示領域
        if(!response.getFile().isEmpty())
        {
            postedValue += "<td class=\"imagearea\">";
            postedValue += "<img src=\"./GetImage?file=" + response.getFile() + "\">";
            postedValue += "</td>" + LFCR;
        }
        // 文章表示領域 ヘッダー
        postedValue += "<td class=\"textarea\">";
        postedValue += "<div>";
        if(resNum > 0)
        {
            postedValue += resNum + " ";
        }
        if(response.getEmail().isEmpty())
        {
            postedValue += response.getName();
        }
        else
        {
            postedValue += "<a href=\"mailto:" + response.getEmail() + "\">";
            postedValue += response.getName();
            postedValue += "</a>";
        }
        postedValue += "　";
        postedValue += "投稿： " + response.getTime();
        postedValue += "</div>" + LFCR;
        // 文章表示領域 内容
        postedValue += "<div class=\"responseContent\">" + LFCR;
        String content = response.getText();
        content = content.replace("\r\n", "<br>");
        content = CommonsText.repliceHyperlink(content);
        postedValue += content + LFCR;
        // form メソッド
        postedValue += "<div class=\"commentPostField\">" + LFCR;
        postedValue += rcManager.createCommentEntryForm(id);
        postedValue += "</div>" + LFCR;
        postedValue += rcManager.createComentDivision(resComments);
        postedValue += "</div>" + LFCR;
        postedValue += "</td>" + LFCR;
        postedValue += "</tr>" + LFCR;
        postedValue += "</table>" + LFCR;
        return postedValue;
    }

    /**
     * idに合致する投稿を取得（無ければnull）
     * @param id 投稿id
     * @return 
     */
    public Response getResponse(int id)
    {
        Response res = null;
        try
        {
            // ドライバクラスをロード
            Class.forName(CommonsDB.MYSQL_DRIVER); // MySQLの場合
            // データベースへ接続
            Connection con = DriverManager.getConnection(SERV_URL, SERV_USER, SERV_PASS);
            // データの取得
            String sql;
            sql = "SELECT * FROM " + TABLE_ENTRY + " WHERE id = ?;";
            PreparedStatement prepStmt = con.prepareStatement(sql);
            prepStmt.setString(1, Integer.toString(id));
            ResultSet resultSet = prepStmt.executeQuery();

            while(resultSet.next())
            {
                id = resultSet.getInt("id");
                String tm = resultSet.getString("time");
                String nm = resultSet.getString("name");
                String tx = resultSet.getString("text");
                String em = resultSet.getString("email");
                String img = resultSet.getString("file");
                nm = CommonsText.repliceWarningWords(nm);
                tx = CommonsText.repliceWarningWords(tx);
                em = CommonsText.repliceWarningWords(em);
                res = new Response(id, tm, nm, tx, em, img);
            }
            
            prepStmt.close();
            con.close();
        }
        catch (ClassNotFoundException ex)
        {
            Logger.getLogger(ResponseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ResponseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            return res;
        }
    }
    
    /**
     * 全ての投稿情報を取得
     * @return 
     */
    public ArrayList<Response> getResponseAll()
    {
        ArrayList<Response> responses = new ArrayList<>();
        responses.clear();
        responses.addAll(Arrays.asList(selectAllResponses()));
        return responses;
    }

    /**
     * XMLファイルを作成
     * @return 
     */
    public Boolean createXMLFile()
    {
        try
        {
            Response[] readedResAry = selectAllResponses();
            String filename = XML_FILE_NAME;
            FileOutputStream fileOStream = new FileOutputStream(filename);
            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(fileOStream));
            encoder.writeObject(readedResAry);
            encoder.close();
            return true;
        }
        catch (FileNotFoundException ex)
        {
            return false;
        }
    }
    
    /**
     * 投稿をDBに追加（失敗したらfalse）
     * @param response
     * @return 
     */
    private boolean insertResponse(Response response)
    {
        boolean isSuccess = false;
        try
        {
            // ドライバクラスをロード
            Class.forName(CommonsDB.MYSQL_DRIVER); // MySQLの場合
            // データベースへ接続
            Connection con = DriverManager.getConnection(SERV_URL, SERV_USER, SERV_PASS);
            // データの追加
            String sql;
            sql = "INSERT INTO " + TABLE_ENTRY + " (time, name, text, email, file) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement prepStmt = con.prepareStatement(sql);
            // 要素の取得
            String time = response.getTime();
            String name = response.getName();
            String text = response.getText();
            String email = response.getEmail();
            String file = response.getFile();
            // SQLの置換・実行
            prepStmt.setString(1, time);
            prepStmt.setString(2, name);
            prepStmt.setString(3, text);
            prepStmt.setString(4, email);
            prepStmt.setString(5, file);
            prepStmt.execute();
            // 終了処理
            isSuccess = true; // close の後におくと実行されない
            prepStmt.close();
            con.close();
        }
        catch (ClassNotFoundException | SQLException ex)
        {
            Logger.getLogger(ResponseManager.class.getName()).log(Level.SEVERE, null, ex);
            isSuccess = false;
        }
        finally
        {
            return isSuccess;
        }
    }
    
    /**
     * バックアップファイルを作成(XML)
     * @return 
     */
    private Boolean makeBuckupFile()
    {
        try
        {
            Response[] readedResAry = selectAllResponses();
            
            String filename = BACKUP_FILE_NAME + (readedResAry.length % 10) + BACKUP_EXTENSION;
            
            FileOutputStream fileOStream = new FileOutputStream(filename);
            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(fileOStream));
            encoder.writeObject(readedResAry);
            encoder.close();
            return true;
        }
        catch (FileNotFoundException ex)
        {
            return false;
        }
    }
    
    /**
     * DBから全ての投稿情報を取得
     * @return ファイルが存在しなければ　null
     */
    private Response[] selectAllResponses()
    {
        ArrayList<Response> aryList = new ArrayList();
        aryList.clear();
        try
        {
            // ドライバクラスをロード
            Class.forName(CommonsDB.MYSQL_DRIVER); // MySQLの場合
            // データベースへ接続
            Connection con = DriverManager.getConnection(SERV_URL, SERV_USER, SERV_PASS);
            
            // データの取得
            String sql2 = "SELECT * FROM entry_tbl";
            Statement stmt2 = con.createStatement();
            ResultSet rs2 = stmt2.executeQuery(sql2);
            Response res;
            while(rs2.next())
            {
                int id = rs2.getInt("id");
                String tm = rs2.getString("time");
                String nm = rs2.getString("name");
                String tx = rs2.getString("text");
                String em = rs2.getString("email");
                String img = rs2.getString("file");
                res = new Response(id, tm, nm, tx, em, img);
                aryList.add(res);
            }
            Response[] resAry = aryList.toArray(new Response[aryList.size()]);
            // 接続解除
            rs2.close();
            stmt2.close();
            con.close();
            return resAry;
        }
        catch (ClassNotFoundException | SQLException ex)
        {
            return null;
        }
    }
    
    private Response escapeXSS(Response res)
    {
        res.setName(CommonsText.repliceWarningWords(res.getName()));
        res.setText(CommonsText.repliceWarningWords(res.getText()));
        res.setEmail(CommonsText.repliceWarningWords(res.getEmail()));
        return res;
    }
}
